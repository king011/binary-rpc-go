package logger

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Logger 日誌
var Logger *zap.Logger
var _atom zap.AtomicLevel

func init() {
	_atom = zap.NewAtomicLevel()
	encoderCfg := zap.NewDevelopmentEncoderConfig()
	core := zapcore.NewCore(
		zapcore.NewConsoleEncoder(encoderCfg),
		os.Stdout,
		_atom,
	)

	_atom.SetLevel(zap.FatalLevel)
	Logger = zap.New(core)
}

// SetLevel alters the logging level.
func SetLevel(l zapcore.Level) {
	_atom.SetLevel(l)
}

// Enable .
func Enable(ok bool) {
	if ok {
		_atom.SetLevel(zapcore.DebugLevel)
	} else {
		_atom.SetLevel(zapcore.FatalLevel)
	}
}
