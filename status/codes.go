package status

// Code 錯誤碼
type Code uint16

const (
	// OK is returned on success.
	OK Code = 0
	// Canceled indicates the operation was canceled (typically by the caller).
	Canceled Code = 1
	// Unknown error. An example of where this error may be returned is
	// if a Status value received from another address space belongs to
	// an error-space that is not known in this address space. Also
	// errors raised by APIs that do not return enough error information
	// may be converted to this error.
	// 未知錯誤 通常是由 API 返回的 錯誤 沒有足夠的信息將錯誤進行分類
	Unknown Code = 2
	// InvalidArgument indicates client specified an invalid argument.
	// Note that this differs from FailedPrecondition. It indicates arguments
	// that are problematic regardless of the state of the system
	// (e.g., a malformed file name).
	// 調用者 傳入了 無效的 參數 系統無法 繼續執行此請求
	InvalidArgument Code = 3
	// DeadlineExceeded means operation expired before completion.
	// For operations that change the state of the system, this error may be
	// returned even if the operation has completed successfully. For
	// example, a successful response from a server could have been delayed
	// long enough for the deadline to expire.
	// 通常是 任務 超時 已經過期
	DeadlineExceeded Code = 4
	// NotFound means some requested entity (e.g., file or directory) was
	// not found.
	// 通常是 調用者請求的 資源 不存在
	NotFound Code = 5
	// AlreadyExists means an attempt to create an entity failed because one
	// already exists.
	// 通常是 調用者 請求創建的 資源 已經存在 所以無法 創建
	AlreadyExists Code = 6
	// PermissionDenied indicates the caller does not have permission to
	// execute the specified operation. It must not be used for rejections
	// caused by exhausting some resource (use ResourceExhausted
	// instead for those errors). It must not be
	// used if the caller cannot be identified (use Unauthenticated
	// instead for those errors).
	// 調用者 沒有權限
	PermissionDenied Code = 7
	// ResourceExhausted indicates some resource has been exhausted, perhaps
	// a per-user quota, or perhaps the entire file system is out of space.
	// 通常是 系統資源 或 分配給調用者的 資源已經耗盡 所以無法 執行 請求
	ResourceExhausted Code = 8
	// FailedPrecondition indicates operation was rejected because the
	// system is not in a state required for the operation's execution.
	// For example, directory to be deleted may be non-empty, an rmdir
	// operation is applied to a non-directory, etc.
	//
	// A litmus test that may help a service implementor in deciding
	// between FailedPrecondition, Aborted, and Unavailable:
	//  (a) Use Unavailable if the client can retry just the failing call.
	//  (b) Use Aborted if the client should retry at a higher-level
	//      (e.g., restarting a read-modify-write sequence).
	//  (c) Use FailedPrecondition if the client should not retry until
	//      the system state has been explicitly fixed. E.g., if an "rmdir"
	//      fails because the directory is non-empty, FailedPrecondition
	//      should be returned since the client should not retry unless
	//      they have first fixed up the directory by deleting files from it.
	//  (d) Use FailedPrecondition if the client performs conditional
	//      REST Get/Update/Delete on a resource and the resource on the
	//      server does not match the condition. E.g., conflicting
	//      read-modify-write on the same resource.
	// 通常 表示 錯誤 並且 客戶端 不應該 進行 重試操作 具體參數 如上  a b c d
	FailedPrecondition Code = 9
	// Aborted indicates the operation was aborted, typically due to a
	// concurrency issue like sequencer check failures, transaction aborts,
	// etc.
	//
	// See litmus test above for deciding between FailedPrecondition,
	// Aborted, and Unavailable.
	// 通常 類似 Unavailable  但如果要重試操作的化 應該使用 比 Unavailable 更高級別的 重試操作
	Aborted Code = 10
	// OutOfRange means operation was attempted past the valid range.
	// E.g., seeking or reading past end of file.
	//
	// Unlike InvalidArgument, this error indicates a problem that may
	// be fixed if the system state changes. For example, a 32-bit file
	// system will generate InvalidArgument if asked to read at an
	// offset that is not in the range [0,2^32-1], but it will generate
	// OutOfRange if asked to read from an offset past the current
	// file size.
	//
	// There is a fair bit of overlap between FailedPrecondition and
	// OutOfRange. We recommend using OutOfRange (the more specific
	// error) when it applies so that callers who are iterating through
	// a space can easily look for an OutOfRange error to detect when
	// they are done.
	// 通常表示 請求的數據已經越界 如操作數組期望的索引 讀取檔案尾之後的 數據
	OutOfRange Code = 11
	// Unimplemented indicates operation is not implemented or not
	// supported/enabled in this service.
	// 通常表示 一個 請求 系統 還未實現 可能會在將來實現
	Unimplemented Code = 12
	// Internal errors. Means some invariants expected by underlying
	// system has been broken. If you see one of these errors,
	// something is very broken.
	// 通常 這個錯誤 意味者 系統已經崩潰 但你不想讓用戶知道 所以還在讓程序 繼續假裝 運行 但整個系統 已經被破壞 無法如預期工作
	Internal Code = 13
	// Unavailable indicates the service is currently unavailable.
	// This is a most likely a transient condition and may be corrected
	// by retrying with a backoff.
	//
	// See litmus test above for deciding between FailedPrecondition,
	// Aborted, and Unavailable.
	//
	// 通常表示 當前服務不可用 但不可用的狀態是短暫的 客戶可以在之後進行 重試操作(例如 網路異常 tcp斷線都會返回 此錯誤)
	Unavailable Code = 14
	// DataLoss indicates unrecoverable data loss or corruption.
	// 通常表示 無法恢復數據 或 數據已經 損毀
	DataLoss Code = 15
	// Unauthenticated indicates the request does not have valid
	// authentication credentials for the operation.
	// 調用者的憑證 有問題 無法識別 調用者的 身份
	Unauthenticated Code = 16
)

func (c Code) String() string {
	switch c {
	case OK:
		return "OK"
	case Canceled:
		return "Canceled"
	case Unknown:
		return "Unknown"
	case InvalidArgument:
		return "InvalidArgument"
	case DeadlineExceeded:
		return "DeadlineExceeded"
	case NotFound:
		return "NotFound"
	case AlreadyExists:
		return "AlreadyExists"
	case PermissionDenied:
		return "PermissionDenied"
	case ResourceExhausted:
		return "ResourceExhausted"
	case FailedPrecondition:
		return "FailedPrecondition"
	case Aborted:
		return "Aborted"
	case OutOfRange:
		return "OutOfRange"
	case Unimplemented:
		return "Unimplemented"
	case Internal:
		return "Internal"
	case Unavailable:
		return "Unavailable"
	case DataLoss:
		return "DataLoss"
	case Unauthenticated:
		return "Unauthenticated"
	}
	return "Unknown"
}
