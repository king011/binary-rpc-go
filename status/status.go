package status

import (
	"fmt"
)

// rpc 錯誤
type statusError struct {
	code    Code
	message string
}

func (se statusError) Error() string {
	return fmt.Sprintf("rpc error: code = %s desc = %s", se.code, se.message)
}

// Error returns an error representing c and msg.  If c is OK, returns nil.
func Error(c Code, msg string) error {
	if c == OK {
		return nil
	}
	return statusError{
		code:    c,
		message: msg,
	}
}

// Errorf returns Error(c, fmt.Sprintf(format, a...)).
func Errorf(c Code, format string, a ...interface{}) error {
	if c == OK {
		return nil
	}
	return statusError{
		code:    c,
		message: fmt.Sprintf(format, a...),
	}
}

// NewError 包裝 錯誤 到 rpc 錯誤
func NewError(e error) error {
	if eg, ok := e.(statusError); ok {
		return eg
	}
	return statusError{
		code:    Unknown,
		message: e.Error(),
	}
}

// GetCode 返回 rpc 錯誤代碼
func GetCode(e error) Code {
	if eg, ok := e.(statusError); ok {
		return eg.code
	}
	return Unknown
}
