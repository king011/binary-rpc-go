package core_test

import (
	"testing"

	"gitlab.com/king011/binary-rpc-go/core"
)

func TestCRC(t *testing.T) {
	str := "cerberus is an idea"
	m := core.Message{
		Data: make([]byte, core.HeaderLen+len(str)),
	}
	copy(m.Data[core.HeaderLen:], str)
	crc16 := m.CRC16()
	if 14477 != crc16 {
		t.Fatal("crc not match 14477,is", crc16)
	}

	str = "this is a test"
	m = core.Message{
		Data: make([]byte, core.HeaderLen+len(str)),
	}
	copy(m.Data[core.HeaderLen:], str)
	crc16 = m.CRC16()
	if 12861 != crc16 {
		t.Fatal("crc not match 12861,is", crc16)
	}
}
