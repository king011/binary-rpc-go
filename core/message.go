package core

import (
	"encoding/binary"
	"math"
)

const (
	// Version 當前協議版本
	Version = 1

	// HeaderLen 包頭長
	HeaderLen = 1 + 2 + 2 + 2

	// RequestEnd 沒有後續數據
	RequestEnd = 1
	// RequestContinue 存在後續數據
	RequestContinue = 2
	// RequestCancel 取消請求
	RequestCancel = 3
)

// Message 網路消息
type Message struct {
	Data []byte
}

// CalculateMessageSize 計算網路數據包 長度
func CalculateMessageSize(validation bool, body int) int {
	if validation {
		return 2 + HeaderLen + body
	}
	return HeaderLen + body
}

// NewMessage 創建 網路消息
func NewMessage(buf []byte, id, cmd uint16, request uint8, validation bool, body []byte) (m Message) {
	bodyLen := len(body)
	size := CalculateMessageSize(validation, bodyLen)
	if len(buf) < size {
		panic("buf < message size ")
	} else if size > math.MaxUint16 {
		panic("message size > max uint16")
	}

	copy(buf[HeaderLen:], body)
	m = Message{
		Data: buf[:size],
	}
	m.SetVersion(Version)
	m.SetRequest(request)
	m.SetValidation(validation)
	m.SetID(id)
	m.SetCommand(cmd)
	m.SetLen(uint16(size))
	if validation {
		binary.BigEndian.PutUint16(buf[len(buf)-2:], m.CRC16())
	}
	return
}

// Version 返回協議版本
func (m Message) Version() uint8 {
	return (m.Data[0] & 0xF8) >> 3
}

// SetVersion 設置協議版本
func (m Message) SetVersion(version uint8) {
	version <<= 3
	m.Data[0] &= 0x7
	m.Data[0] |= version
}

// Validation 返回是否使用 CRC
func (m Message) Validation() bool {
	return m.Data[0]&0x4 != 0
}

// SetValidation 設置是否使用 CRC
func (m Message) SetValidation(yes bool) {
	if yes {
		m.Data[0] |= 0x4
	} else {
		m.Data[0] &= 0xFB
	}
}

// Request 返回數據類型
func (m Message) Request() uint8 {
	return m.Data[0] & 0x3
}

// SetRequest 設置數據類型
func (m Message) SetRequest(v uint8) {
	v &= 0x3
	m.Data[0] |= v
}

// ID 返回請求邏輯號
func (m Message) ID() uint16 {
	return binary.BigEndian.Uint16(m.Data[1:])
}

// SetID 設置請求邏輯號
func (m Message) SetID(id uint16) {
	binary.BigEndian.PutUint16(m.Data[1:], id)
}

// Len 返回包長
func (m Message) Len() uint16 {
	return binary.BigEndian.Uint16(m.Data[1+2:])
}

// SetLen 設置包長
func (m Message) SetLen(length uint16) {
	binary.BigEndian.PutUint16(m.Data[1+2:], length)
}

// Command 返回指令
func (m Message) Command() uint16 {
	return binary.BigEndian.Uint16(m.Data[1+2+2:])
}

// SetCommand 設置指令
func (m Message) SetCommand(cmd uint16) {
	binary.BigEndian.PutUint16(m.Data[1+2+2:], cmd)
}

// Body .
func (m Message) Body() (body []byte) {
	if m.Validation() {
		body = m.Data[HeaderLen : len(m.Data)-2]
	} else {
		body = m.Data[HeaderLen:]
	}
	return
}

// Bottom .
func (m Message) Bottom() (crc uint16) {
	if m.Validation() {
		crc = binary.BigEndian.Uint16(m.Data[len(m.Data)-2:])
	}
	return
}

// CRC16 計算 crc16
func (m Message) CRC16() (crc uint16) {
	size := len(m.Data)
	if m.Validation() {
		size -= 2
	}
	crc = 0xffff
	for i := 0; i < size; i++ {
		tmp := uint16(m.Data[i])
		crc = crc ^ tmp<<8
		for j := 0; j < 8; j++ {
			if crc&0x8000 != 0 {
				crc = crc<<1 ^ 0x1021
			} else {
				crc <<= 1
			}
		}
	}
	crc &= 0xFFFF
	return
}
