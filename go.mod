module gitlab.com/king011/binary-rpc-go

go 1.12

require (
	gitlab.com/king011/binary-go v0.1.0
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.2.0 // indirect
	go.uber.org/zap v1.11.0
)
