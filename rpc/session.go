package rpc

import (
	"errors"
	"fmt"
	"io"
	"net"
	"sync"

	"go.uber.org/zap"

	"gitlab.com/king011/binary-rpc-go/core"
	"gitlab.com/king011/binary-rpc-go/logger"
)

// Session tcp 會話
type Session struct {
	c    net.Conn
	opts *options

	isStoped bool
	stoped   chan struct{}
	mutex    sync.Mutex

	chWriet chan []byte

	// 用戶數據
	Keys map[string]interface{}
	// 讀寫鎖 操作用戶數據時 應該加鎖
	sync.RWMutex

	// 基本方法
	methods map[uint16]MethodDesc
	// rpc 控制器
	controllers map[uint16]*_Controller
}

func newSession(c net.Conn, opts *options, methods map[uint16]MethodDesc) *Session {
	return &Session{
		c:    c,
		opts: opts,

		chWriet: make(chan []byte, 10),
		stoped:  make(chan struct{}),

		methods:     methods,
		controllers: make(map[uint16]*_Controller),
	}
}
func (s *Session) stop() {
	s.mutex.Lock()
	if !s.isStoped {
		s.isStoped = true
		close(s.stoped)
		s.c.Close()
		for key, controller := range s.controllers {
			delete(s.controllers, key)
			controller.stop()
		}
	}
	s.mutex.Unlock()
}
func (s *Session) write() {
	running := true
	for running {
		select {
		case <-s.stoped:
			running = false
		case data := <-s.chWriet:
			_, e := s.c.Write(data)
			if e != nil {
				if ce := logger.Logger.Check(zap.DebugLevel, "session write"); ce != nil {
					ce.Write(
						zap.Error(e),
					)
				}
				running = false
				s.stop()
			}
		}
	}
}
func (s *Session) run() {
	go s.write()
	var msg core.Message
	var e error
	for {
		msg, e = s.readMessage()
		if e != nil {
			break
		}
		e = s.handlerMessage(msg)
		if e != nil {
			break
		}
	}
	s.stop()
}
func (s *Session) handlerMessage(msg core.Message) (e error) {
	id := msg.ID()
	var controller *_Controller
	var ok bool
	s.mutex.Lock()
	if controller, ok = s.controllers[id]; !ok {
		r := msg.Request()
		switch r {
		case core.RequestEnd:
		case core.RequestContinue:
		case core.RequestCancel:
			s.mutex.Unlock()
			if ce := logger.Logger.Check(zap.WarnLevel, "controllers not exits,not support cancel"); ce != nil {
				ce.Write(
					zap.Uint16("id", id),
				)
			}
			return
		default:
			s.mutex.Unlock()
			if ce := logger.Logger.Check(zap.WarnLevel, "not support request"); ce != nil {
				ce.Write(
					zap.Uint16("id", id),
					zap.Uint8("request", r),
				)
			}
			return
		}

		// 創建新控制器
		validation := msg.Validation()
		controller, er := newController(s, id, msg.Command(), validation,
			s.methods,
		)
		if er != nil {
			e = s.responseError(id, validation, er)
			return
		}
		s.controllers[id] = controller
		go controller.run()
	}
	s.mutex.Unlock()

	// 路由消息
	e = controller.pushRequest(msg)
	return
}
func (s *Session) removeController(c *_Controller) {
	s.mutex.Lock()
	if controller, ok := s.controllers[c.id]; ok && controller == c {
		delete(s.controllers, c.id)
	}
	s.mutex.Unlock()
}
func (s *Session) responseError(id uint16, validation bool, er error) (e error) {
	code := uint16(Code(er))
	msg := er.Error()
	var buf []byte
	if validation {
		buf = make([]byte, core.HeaderLen+len(msg)+2)
	} else {
		buf = make([]byte, core.HeaderLen+len(msg))
	}
	m := core.NewMessage(buf, id, code, core.RequestEnd, validation, []byte(msg))
	s.Write(m.Data)
	return
}
func (s *Session) Write(b []byte) {
	select {
	case <-s.stoped:
	case s.chWriet <- b:
	}
}
func (s *Session) readMessage() (msg core.Message, e error) {
	buf := make([]byte, s.opts.maxBody+9)
	// 讀取 header
	_, e = io.ReadFull(s.c, buf[:7])
	if e != nil {
		return
	}
	m := core.Message{Data: buf}
	// 驗證版本
	version := m.Version()
	if version != core.Version {
		e = fmt.Errorf("current version is %v,not support %v", core.Version, version)
		return
	}

	// 獲取包長
	length := m.Len()
	validation := m.Validation()
	if validation {
		if length < core.HeaderLen+2 {
			e = errors.New("bad message length")
			return
		} else if length-core.HeaderLen-2 > s.opts.maxBody {
			e = errors.New("message body length > max length")
			return
		}
	} else if length < core.HeaderLen {
		e = errors.New("bad message length")
		return
	} else if length-core.HeaderLen > s.opts.maxBody {
		e = errors.New("message body length > max length")
		return
	}

	// 讀取包
	_, e = io.ReadFull(s.c, m.Data[core.HeaderLen:int(length)-core.HeaderLen])
	if e != nil {
		return
	}
	// 驗證 crc
	if validation {
		if m.CRC16() != m.Bottom() {
			e = errors.New("crc not match")
			return
		}
	}
	return
}

// HasKey 返回是否存在 指定 key 此函數會自動加鎖 RWMutex
func (s *Session) HasKey(key string) (ok bool) {
	s.RWMutex.RLock()
	if s.Keys != nil {
		_, ok = s.Keys[key]
	}
	s.RWMutex.RUnlock()
	return
}

// GetKey 返回 指定 key 的值 此函數會自動加鎖 RWMutex
func (s *Session) GetKey(key string) (val interface{}, ok bool) {
	s.RWMutex.RLock()
	if s.Keys != nil {
		val, ok = s.Keys[key]
	}
	s.RWMutex.RUnlock()
	return
}

// SetKey 設置 key 的值 此函數會自動加鎖 RWMutex
func (s *Session) SetKey(key string, val interface{}) {
	s.RWMutex.Lock()
	if s.Keys == nil {
		s.Keys = make(map[string]interface{})
	}
	s.Keys[key] = val
	s.RWMutex.Unlock()
	return
}
