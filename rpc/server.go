package rpc

import (
	"context"
	"fmt"
	"net"
	"sync"

	binary_core "gitlab.com/king011/binary-go/core"
	"gitlab.com/king011/binary-rpc-go/logger"
	"go.uber.org/zap"
)

var binaryContext = binary_core.Background()

// Server rpc 服務器
type Server struct {
	stoped    chan struct{}
	listener  net.Listener
	waitGroup sync.WaitGroup
	opts      options
	sync.Mutex

	methods map[uint16]MethodDesc
}

// NewServer 創建 rpc 服務器
func NewServer(opt ...ServerOption) *Server {
	opts := defaultServerOptions
	for _, o := range opt {
		o(&opts)
	}

	return &Server{
		stoped:  make(chan struct{}),
		opts:    opts,
		methods: make(map[uint16]MethodDesc),
	}
}

// Stop 停止服務器 並取消所有 執行中的 方法
func (s *Server) Stop() {
	s.Lock()
	if s.listener != nil {
		if ce := logger.Logger.Check(zap.InfoLevel, "Server Stop"); ce != nil {
			ce.Write(
				zap.String("addr", s.listener.Addr().String()),
			)
		}

		s.listener.Close()
		s.listener = nil
		close(s.stoped)
	}
	s.Unlock()
}

// Serve 運行服務 直到 Stop() 且所有方法返回
func (s *Server) Serve(l net.Listener) {
	s.Lock()
	s.listener = l
	s.Unlock()

	if ce := logger.Logger.Check(zap.InfoLevel, "Server Serve"); ce != nil {
		ce.Write(
			zap.String("addr", l.Addr().String()),
		)
	}
	for {
		c, e := l.Accept()
		if e != nil {
			if s.IsStoped() {
				break
			} else {
				continue
			}
		}
		s.waitGroup.Add(1)
		go s.onAccept(c)
	}

	s.waitGroup.Wait()
}

// IsStoped 返回 服務器 是否關閉
func (s *Server) IsStoped() (yes bool) {
	select {
	case <-s.stoped:
		yes = true
	default:
	}
	return
}
func (s *Server) onAccept(c net.Conn) {
	defer s.waitGroup.Done()
	session := newSession(c, &s.opts, s.methods)
	go s.waitAndStopSession(session)
	session.run()
	s.waitGroup.Done()
}
func (s *Server) waitAndStopSession(session *Session) {
	select {
	case <-s.stoped:
		session.stop()
	case <-session.stoped:
	}
}

type options struct {
	maxBody uint16
}

var defaultServerOptions = options{
	maxBody: 1024 * 32,
}

// ServerOption 設置服務器 選項
type ServerOption func(*options)

// MaxBody 設置最大 body
func MaxBody(max uint16) ServerOption {
	return func(opt *options) {
		if max > 128 {
			opt.maxBody = max
		}
	}
}

type methodHandler func(ctx context.Context, session *Session, b binary_core.Context, body []byte) (binary_core.Type, error)

// MethodDesc represents an RPC service's method specification.
type MethodDesc struct {
	Command uint16
	Handler methodHandler
}

// ServiceDesc represents an RPC service's specification.
type ServiceDesc struct {
	Methods []MethodDesc
}

// RegisterService 註冊 服務
func (s *Server) RegisterService(sd *ServiceDesc) {
	for i := 0; i < len(sd.Methods); i++ {
		cmd := sd.Methods[i].Command
		if _, ok := s.methods[cmd]; ok {
			panic(fmt.Sprintf("commnad already exits <%v>", cmd))
		}
		s.methods[cmd] = sd.Methods[i]
	}
}
