package rpc

import (
	"gitlab.com/king011/binary-rpc-go/status"
)

// Code 返回 rpc 錯誤碼
func Code(e error) status.Code {
	return status.GetCode(e)
}

// Error returns an error representing c and msg.  If c is OK, returns nil.
func Error(c status.Code, msg string) error {
	return status.Error(c, msg)
}

// Errorf returns Error(c, fmt.Sprintf(format, a...)).
func Errorf(c status.Code, format string, a ...interface{}) error {
	return status.Errorf(c, format, a...)
}
