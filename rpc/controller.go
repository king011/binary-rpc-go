package rpc

import (
	"context"
	"errors"
	"math"
	"sync"

	"gitlab.com/king011/binary-rpc-go/status"

	"gitlab.com/king011/binary-rpc-go/core"
	"gitlab.com/king011/binary-rpc-go/logger"
	"go.uber.org/zap"
)

type _Controller struct {
	session    *Session
	id         uint16
	cmd        uint16
	validation bool
	ctx        context.Context
	cancel     context.CancelFunc
	sync.Mutex

	method *MethodDesc

	request       chan core.Message
	requestClosed bool
}

func newController(session *Session, id, cmd uint16, validation bool, methods map[uint16]MethodDesc) (c *_Controller, e error) {
	var method *MethodDesc
	var requestCache int
	if m, ok := methods[cmd]; ok {
		method = &m
		requestCache = 1
	}

	if method == nil {
		e = Errorf(status.Unimplemented, "not support cmd <%v>", cmd)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	c = &_Controller{
		session: session,

		id:         id,
		cmd:        cmd,
		validation: validation,

		ctx:    ctx,
		cancel: cancel,

		request: make(chan core.Message, requestCache),
		method:  method,
	}
	return
}
func (c *_Controller) run() {
	done := c.ctx.Done()
	running := true
	for running {
		select {
		case <-done:
			c.session.removeController(c)
			running = false
		case msg := <-c.request:
			if c.method != nil {
				c.doneMethod(msg)
				c.session.removeController(c)
				running = false
			}
		}
	}
}
func (c *_Controller) doneMethod(msg core.Message) {
	response, e := c.method.Handler(c.ctx, c.session, binaryContext, msg.Body())
	if e != nil {
		e = c.session.responseError(c.id, c.validation, e)
		if e != nil {
			c.session.stop()
		}
		return
	}

	// 計算包長
	var use int
	if c.validation {
		use = core.HeaderLen
	} else {
		use = core.HeaderLen + 2
	}
	size, e := response.MarshalSize(binaryContext, use)
	if e != nil {
		c.session.stop()
		return
	}
	if size > math.MaxUint16 {
		e = Error(status.OutOfRange, "response message size > max uint16")
		return
	} else if size > int(c.session.opts.maxBody) {
		e = Error(status.OutOfRange, "response body size > max body")
		return
	}

	// 響應
	buf := make([]byte, size)
	_, e = response.MarshalToBuffer(binaryContext, buf[core.HeaderLen:], 0)
	if e != nil {
		return
	}
	m := core.Message{
		Data: buf,
	}
	m.SetVersion(core.Version)
	m.SetValidation(c.validation)
	m.SetRequest(core.RequestEnd)
	m.SetID(c.id)
	m.SetCommand(uint16(status.OK))
	m.SetLen(uint16(size))
	c.session.Write(m.Data)
}
func (c *_Controller) stop() {
	c.Lock()
	if c.cancel != nil {
		c.cancel()
		c.cancel = nil
	}
	c.Unlock()
}
func (c *_Controller) pushRequest(msg core.Message) (e error) {
	c.Lock()
	if c.cancel == nil {
		if ce := logger.Logger.Check(zap.DebugLevel, "controllerer stoped"); ce != nil {
			ce.Write(
				zap.Uint16("id", c.id),
			)
		}
	} else {
		if c.requestClosed {
			e = errors.New("request closed")
		} else {
			c.request <- msg
		}
	}
	c.Unlock()
	return
}
