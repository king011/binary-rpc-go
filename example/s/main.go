package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"runtime"

	rpc_zoo "gitlab.com/king011/binary-rpc-go/example/protocol/zoo"
	"gitlab.com/king011/binary-rpc-go/logger"
	"gitlab.com/king011/binary-rpc-go/rpc"
)

// Addr 工作地址
const Addr = ":7000"

func main() {
	logger.Enable(true)
	// 監聽 tcp
	l, e := net.Listen("tcp", Addr)
	if e != nil {
		log.Fatalln(e)
	}

	// 創建服務器
	srv := rpc.NewServer()

	// 註冊 服務
	rpc_zoo.RegisterServiceServer(srv, _Zoo{})

	go runCmd(srv)
	// 執行 服務 並且 等待 結束
	srv.Serve(l)
}
func runCmd(srv *rpc.Server) {
	var cmd string
	for {
		fmt.Print("$>")
		fmt.Scan(&cmd)
		if cmd == "q" {
			break
		} else if cmd == "go" {
			fmt.Println("NumGoroutine", runtime.NumGoroutine())
		}
	}
	srv.Stop()
}

type _Zoo struct {
}

func (_Zoo) Cat(ctx context.Context, session *rpc.Session, request *rpc_zoo.CatRequest) (response *rpc_zoo.CatResponse, e error) {
	fmt.Println("Cat id", request.ID)
	switch request.ID {
	case 1:
		response = &rpc_zoo.CatResponse{
			Name: "cat 1",
		}
	case 2:
		response = &rpc_zoo.CatResponse{
			Name: "cat 2",
		}
	default:
		e = fmt.Errorf("unknow id")
		return
	}
	return
}
