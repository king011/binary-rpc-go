package zoo

import (
	"context"

	binary_core "gitlab.com/king011/binary-go/core"
	"gitlab.com/king011/binary-rpc-go/rpc"
)

// ServiceServer 服務器接口
type ServiceServer interface {
	Cat(ctx context.Context, session *rpc.Session, request *CatRequest) (response *CatResponse, e error)
}

// RegisterServiceServer 註冊接口到 rpc 服務
func RegisterServiceServer(s *rpc.Server, srv ServiceServer) {
	s.RegisterService(&rpc.ServiceDesc{
		Methods: []rpc.MethodDesc{
			rpc.MethodDesc{
				Command: 1,
				Handler: func(ctx context.Context, session *rpc.Session, b binary_core.Context, body []byte) (binary_core.Type, error) {
					var request CatRequest
					e := request.Unmarshal(b, body)
					if e != nil {
						return nil, e
					}
					return srv.Cat(ctx, session, nil)
				},
			},
		},
	})
}
