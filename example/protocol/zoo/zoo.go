package zoo

import (
	"gitlab.com/king011/binary-go/core"
)

// BirdRequest .
type BirdRequest struct {
	ID	int64
}

// Reset .
func (t *BirdRequest) Reset() {
	t.ID = 0
}

// Marshal0SizeBirdRequests .
func Marshal0SizeBirdRequests(ctx core.Context, use int, arrs []*BirdRequest) (int, error) {
	if len(arrs) != 0 {
		var e error
		use, e = core.MarshalSizeAdd(ctx, use, 4)
		if e != nil {
			return 0, e
		}
		for _, v := range arrs {
			use, e = core.MarshalSizeAdd(ctx, use, 2)
			if e != nil {
				return 0, e
			}
			if v != nil {
				use, e = v.MarshalSize(ctx, use)
				if e != nil {
					return 0, e
				}
			}
		}
	}
	return use, nil
}

// MarshalSize .
func (t *BirdRequest) MarshalSize(ctx core.Context, use int) (size int, e error) {
	use, e = core.MarshalInt64Size(ctx, use, t.ID)
	if e != nil {
		return
	}
	size = use
	return
}

// Marshal0BirdRequests .
func Marshal0BirdRequests(ctx core.Context, buffer []byte, use int, arrs []*BirdRequest) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	var start int
	order := ctx.ByteOrder()
	for _, v:=range arrs {
		start = use
		use, e = core.MarshalAdd(ctx, use, 2, len(buffer))
		if e != nil {
			return
		}
		if v == nil {
			order.PutUint16(buffer[start:], 0)
		} else {
			use, e = v.MarshalToBuffer(ctx, buffer, use)
			if e != nil {
				return
			}
			order.PutUint16(buffer[start:], uint16(use-start-2))
		}
	}
	size = use
	return
}

// Marshal .
func (t *BirdRequest) Marshal(ctx core.Context) (buffer []byte, e error) {
	size, e := t.MarshalSize(ctx, 0)
	if e != nil {
		return
	}
	if size == 0 {
		return
	}
	buffer = make([]byte, size)
	_, e = t.MarshalToBuffer(ctx, buffer, 0)
	if e != nil {
		buffer = nil
		return
	}
	return
}

// MarshalToBuffer .
func (t *BirdRequest) MarshalToBuffer(ctx core.Context, buffer []byte, use int) (size int, e error) {
	use, e = core.MarshalInt64(ctx, buffer, use, 1, t.ID)
	if e != nil {
		return
	}
	size = use
	return
}

// Unmarshal0BirdRequests .
func Unmarshal0BirdRequests(ctx core.Context, buffer []byte) (arrs []*BirdRequest, e error) {
	items, e := core.UnmarshalCount(ctx, buffer)
	if e != nil || len(items) == 0 {
		return
	}
	arrs = make([]*BirdRequest, len(items))
	for i, item := range items {
		if len(item) != 0 {
			var tmp BirdRequest
			e = tmp.Unmarshal(ctx, item)
			if e != nil {
				arrs = nil
				return
			}
			arrs[i] = &tmp
		}
	}
	return
}

// Unmarshal .
func (t *BirdRequest) Unmarshal(ctx core.Context, buffer []byte) (e error) {
	var fields [1]core.Field
	fields[0].ID = 1
	e = core.UnmarshalFields(ctx, buffer, fields[:])
	if e != nil {
		return
	}
	t.ID = core.UnmarshalInt64(ctx, fields[0])

	return
}

// BirdResponse .
type BirdResponse struct {
	Name	string
}

// Reset .
func (t *BirdResponse) Reset() {
	t.Name = ""
}

// Marshal0SizeBirdResponses .
func Marshal0SizeBirdResponses(ctx core.Context, use int, arrs []*BirdResponse) (int, error) {
	if len(arrs) != 0 {
		var e error
		use, e = core.MarshalSizeAdd(ctx, use, 4)
		if e != nil {
			return 0, e
		}
		for _, v := range arrs {
			use, e = core.MarshalSizeAdd(ctx, use, 2)
			if e != nil {
				return 0, e
			}
			if v != nil {
				use, e = v.MarshalSize(ctx, use)
				if e != nil {
					return 0, e
				}
			}
		}
	}
	return use, nil
}

// MarshalSize .
func (t *BirdResponse) MarshalSize(ctx core.Context, use int) (size int, e error) {
	use, e = core.MarshalStringSize(ctx, use, t.Name)
	if e != nil {
		return
	}
	size = use
	return
}

// Marshal0BirdResponses .
func Marshal0BirdResponses(ctx core.Context, buffer []byte, use int, arrs []*BirdResponse) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	var start int
	order := ctx.ByteOrder()
	for _, v:=range arrs {
		start = use
		use, e = core.MarshalAdd(ctx, use, 2, len(buffer))
		if e != nil {
			return
		}
		if v == nil {
			order.PutUint16(buffer[start:], 0)
		} else {
			use, e = v.MarshalToBuffer(ctx, buffer, use)
			if e != nil {
				return
			}
			order.PutUint16(buffer[start:], uint16(use-start-2))
		}
	}
	size = use
	return
}

// Marshal .
func (t *BirdResponse) Marshal(ctx core.Context) (buffer []byte, e error) {
	size, e := t.MarshalSize(ctx, 0)
	if e != nil {
		return
	}
	if size == 0 {
		return
	}
	buffer = make([]byte, size)
	_, e = t.MarshalToBuffer(ctx, buffer, 0)
	if e != nil {
		buffer = nil
		return
	}
	return
}

// MarshalToBuffer .
func (t *BirdResponse) MarshalToBuffer(ctx core.Context, buffer []byte, use int) (size int, e error) {
	use, e = core.MarshalString(ctx, buffer, use, 1, t.Name)
	if e != nil {
		return
	}
	size = use
	return
}

// Unmarshal0BirdResponses .
func Unmarshal0BirdResponses(ctx core.Context, buffer []byte) (arrs []*BirdResponse, e error) {
	items, e := core.UnmarshalCount(ctx, buffer)
	if e != nil || len(items) == 0 {
		return
	}
	arrs = make([]*BirdResponse, len(items))
	for i, item := range items {
		if len(item) != 0 {
			var tmp BirdResponse
			e = tmp.Unmarshal(ctx, item)
			if e != nil {
				arrs = nil
				return
			}
			arrs[i] = &tmp
		}
	}
	return
}

// Unmarshal .
func (t *BirdResponse) Unmarshal(ctx core.Context, buffer []byte) (e error) {
	var fields [1]core.Field
	fields[0].ID = 1
	e = core.UnmarshalFields(ctx, buffer, fields[:])
	if e != nil {
		return
	}
	t.Name = core.UnmarshalString(ctx, fields[0])

	return
}

// CatRequest .
type CatRequest struct {
	ID	int64
}

// Reset .
func (t *CatRequest) Reset() {
	t.ID = 0
}

// Marshal0SizeCatRequests .
func Marshal0SizeCatRequests(ctx core.Context, use int, arrs []*CatRequest) (int, error) {
	if len(arrs) != 0 {
		var e error
		use, e = core.MarshalSizeAdd(ctx, use, 4)
		if e != nil {
			return 0, e
		}
		for _, v := range arrs {
			use, e = core.MarshalSizeAdd(ctx, use, 2)
			if e != nil {
				return 0, e
			}
			if v != nil {
				use, e = v.MarshalSize(ctx, use)
				if e != nil {
					return 0, e
				}
			}
		}
	}
	return use, nil
}

// MarshalSize .
func (t *CatRequest) MarshalSize(ctx core.Context, use int) (size int, e error) {
	use, e = core.MarshalInt64Size(ctx, use, t.ID)
	if e != nil {
		return
	}
	size = use
	return
}

// Marshal0CatRequests .
func Marshal0CatRequests(ctx core.Context, buffer []byte, use int, arrs []*CatRequest) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	var start int
	order := ctx.ByteOrder()
	for _, v:=range arrs {
		start = use
		use, e = core.MarshalAdd(ctx, use, 2, len(buffer))
		if e != nil {
			return
		}
		if v == nil {
			order.PutUint16(buffer[start:], 0)
		} else {
			use, e = v.MarshalToBuffer(ctx, buffer, use)
			if e != nil {
				return
			}
			order.PutUint16(buffer[start:], uint16(use-start-2))
		}
	}
	size = use
	return
}

// Marshal .
func (t *CatRequest) Marshal(ctx core.Context) (buffer []byte, e error) {
	size, e := t.MarshalSize(ctx, 0)
	if e != nil {
		return
	}
	if size == 0 {
		return
	}
	buffer = make([]byte, size)
	_, e = t.MarshalToBuffer(ctx, buffer, 0)
	if e != nil {
		buffer = nil
		return
	}
	return
}

// MarshalToBuffer .
func (t *CatRequest) MarshalToBuffer(ctx core.Context, buffer []byte, use int) (size int, e error) {
	use, e = core.MarshalInt64(ctx, buffer, use, 1, t.ID)
	if e != nil {
		return
	}
	size = use
	return
}

// Unmarshal0CatRequests .
func Unmarshal0CatRequests(ctx core.Context, buffer []byte) (arrs []*CatRequest, e error) {
	items, e := core.UnmarshalCount(ctx, buffer)
	if e != nil || len(items) == 0 {
		return
	}
	arrs = make([]*CatRequest, len(items))
	for i, item := range items {
		if len(item) != 0 {
			var tmp CatRequest
			e = tmp.Unmarshal(ctx, item)
			if e != nil {
				arrs = nil
				return
			}
			arrs[i] = &tmp
		}
	}
	return
}

// Unmarshal .
func (t *CatRequest) Unmarshal(ctx core.Context, buffer []byte) (e error) {
	var fields [1]core.Field
	fields[0].ID = 1
	e = core.UnmarshalFields(ctx, buffer, fields[:])
	if e != nil {
		return
	}
	t.ID = core.UnmarshalInt64(ctx, fields[0])

	return
}

// CatResponse .
type CatResponse struct {
	Name	string
}

// Reset .
func (t *CatResponse) Reset() {
	t.Name = ""
}

// Marshal0SizeCatResponses .
func Marshal0SizeCatResponses(ctx core.Context, use int, arrs []*CatResponse) (int, error) {
	if len(arrs) != 0 {
		var e error
		use, e = core.MarshalSizeAdd(ctx, use, 4)
		if e != nil {
			return 0, e
		}
		for _, v := range arrs {
			use, e = core.MarshalSizeAdd(ctx, use, 2)
			if e != nil {
				return 0, e
			}
			if v != nil {
				use, e = v.MarshalSize(ctx, use)
				if e != nil {
					return 0, e
				}
			}
		}
	}
	return use, nil
}

// MarshalSize .
func (t *CatResponse) MarshalSize(ctx core.Context, use int) (size int, e error) {
	use, e = core.MarshalStringSize(ctx, use, t.Name)
	if e != nil {
		return
	}
	size = use
	return
}

// Marshal0CatResponses .
func Marshal0CatResponses(ctx core.Context, buffer []byte, use int, arrs []*CatResponse) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	var start int
	order := ctx.ByteOrder()
	for _, v:=range arrs {
		start = use
		use, e = core.MarshalAdd(ctx, use, 2, len(buffer))
		if e != nil {
			return
		}
		if v == nil {
			order.PutUint16(buffer[start:], 0)
		} else {
			use, e = v.MarshalToBuffer(ctx, buffer, use)
			if e != nil {
				return
			}
			order.PutUint16(buffer[start:], uint16(use-start-2))
		}
	}
	size = use
	return
}

// Marshal .
func (t *CatResponse) Marshal(ctx core.Context) (buffer []byte, e error) {
	size, e := t.MarshalSize(ctx, 0)
	if e != nil {
		return
	}
	if size == 0 {
		return
	}
	buffer = make([]byte, size)
	_, e = t.MarshalToBuffer(ctx, buffer, 0)
	if e != nil {
		buffer = nil
		return
	}
	return
}

// MarshalToBuffer .
func (t *CatResponse) MarshalToBuffer(ctx core.Context, buffer []byte, use int) (size int, e error) {
	use, e = core.MarshalString(ctx, buffer, use, 1, t.Name)
	if e != nil {
		return
	}
	size = use
	return
}

// Unmarshal0CatResponses .
func Unmarshal0CatResponses(ctx core.Context, buffer []byte) (arrs []*CatResponse, e error) {
	items, e := core.UnmarshalCount(ctx, buffer)
	if e != nil || len(items) == 0 {
		return
	}
	arrs = make([]*CatResponse, len(items))
	for i, item := range items {
		if len(item) != 0 {
			var tmp CatResponse
			e = tmp.Unmarshal(ctx, item)
			if e != nil {
				arrs = nil
				return
			}
			arrs[i] = &tmp
		}
	}
	return
}

// Unmarshal .
func (t *CatResponse) Unmarshal(ctx core.Context, buffer []byte) (e error) {
	var fields [1]core.Field
	fields[0].ID = 1
	e = core.UnmarshalFields(ctx, buffer, fields[:])
	if e != nil {
		return
	}
	t.Name = core.UnmarshalString(ctx, fields[0])

	return
}

// DogRequest .
type DogRequest struct {
	Count	int64
}

// Reset .
func (t *DogRequest) Reset() {
	t.Count = 0
}

// Marshal0SizeDogRequests .
func Marshal0SizeDogRequests(ctx core.Context, use int, arrs []*DogRequest) (int, error) {
	if len(arrs) != 0 {
		var e error
		use, e = core.MarshalSizeAdd(ctx, use, 4)
		if e != nil {
			return 0, e
		}
		for _, v := range arrs {
			use, e = core.MarshalSizeAdd(ctx, use, 2)
			if e != nil {
				return 0, e
			}
			if v != nil {
				use, e = v.MarshalSize(ctx, use)
				if e != nil {
					return 0, e
				}
			}
		}
	}
	return use, nil
}

// MarshalSize .
func (t *DogRequest) MarshalSize(ctx core.Context, use int) (size int, e error) {
	use, e = core.MarshalInt64Size(ctx, use, t.Count)
	if e != nil {
		return
	}
	size = use
	return
}

// Marshal0DogRequests .
func Marshal0DogRequests(ctx core.Context, buffer []byte, use int, arrs []*DogRequest) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	var start int
	order := ctx.ByteOrder()
	for _, v:=range arrs {
		start = use
		use, e = core.MarshalAdd(ctx, use, 2, len(buffer))
		if e != nil {
			return
		}
		if v == nil {
			order.PutUint16(buffer[start:], 0)
		} else {
			use, e = v.MarshalToBuffer(ctx, buffer, use)
			if e != nil {
				return
			}
			order.PutUint16(buffer[start:], uint16(use-start-2))
		}
	}
	size = use
	return
}

// Marshal .
func (t *DogRequest) Marshal(ctx core.Context) (buffer []byte, e error) {
	size, e := t.MarshalSize(ctx, 0)
	if e != nil {
		return
	}
	if size == 0 {
		return
	}
	buffer = make([]byte, size)
	_, e = t.MarshalToBuffer(ctx, buffer, 0)
	if e != nil {
		buffer = nil
		return
	}
	return
}

// MarshalToBuffer .
func (t *DogRequest) MarshalToBuffer(ctx core.Context, buffer []byte, use int) (size int, e error) {
	use, e = core.MarshalInt64(ctx, buffer, use, 1, t.Count)
	if e != nil {
		return
	}
	size = use
	return
}

// Unmarshal0DogRequests .
func Unmarshal0DogRequests(ctx core.Context, buffer []byte) (arrs []*DogRequest, e error) {
	items, e := core.UnmarshalCount(ctx, buffer)
	if e != nil || len(items) == 0 {
		return
	}
	arrs = make([]*DogRequest, len(items))
	for i, item := range items {
		if len(item) != 0 {
			var tmp DogRequest
			e = tmp.Unmarshal(ctx, item)
			if e != nil {
				arrs = nil
				return
			}
			arrs[i] = &tmp
		}
	}
	return
}

// Unmarshal .
func (t *DogRequest) Unmarshal(ctx core.Context, buffer []byte) (e error) {
	var fields [1]core.Field
	fields[0].ID = 1
	e = core.UnmarshalFields(ctx, buffer, fields[:])
	if e != nil {
		return
	}
	t.Count = core.UnmarshalInt64(ctx, fields[0])

	return
}

// DogResponse .
type DogResponse struct {
	Name	string
}

// Reset .
func (t *DogResponse) Reset() {
	t.Name = ""
}

// Marshal0SizeDogResponses .
func Marshal0SizeDogResponses(ctx core.Context, use int, arrs []*DogResponse) (int, error) {
	if len(arrs) != 0 {
		var e error
		use, e = core.MarshalSizeAdd(ctx, use, 4)
		if e != nil {
			return 0, e
		}
		for _, v := range arrs {
			use, e = core.MarshalSizeAdd(ctx, use, 2)
			if e != nil {
				return 0, e
			}
			if v != nil {
				use, e = v.MarshalSize(ctx, use)
				if e != nil {
					return 0, e
				}
			}
		}
	}
	return use, nil
}

// MarshalSize .
func (t *DogResponse) MarshalSize(ctx core.Context, use int) (size int, e error) {
	use, e = core.MarshalStringSize(ctx, use, t.Name)
	if e != nil {
		return
	}
	size = use
	return
}

// Marshal0DogResponses .
func Marshal0DogResponses(ctx core.Context, buffer []byte, use int, arrs []*DogResponse) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	var start int
	order := ctx.ByteOrder()
	for _, v:=range arrs {
		start = use
		use, e = core.MarshalAdd(ctx, use, 2, len(buffer))
		if e != nil {
			return
		}
		if v == nil {
			order.PutUint16(buffer[start:], 0)
		} else {
			use, e = v.MarshalToBuffer(ctx, buffer, use)
			if e != nil {
				return
			}
			order.PutUint16(buffer[start:], uint16(use-start-2))
		}
	}
	size = use
	return
}

// Marshal .
func (t *DogResponse) Marshal(ctx core.Context) (buffer []byte, e error) {
	size, e := t.MarshalSize(ctx, 0)
	if e != nil {
		return
	}
	if size == 0 {
		return
	}
	buffer = make([]byte, size)
	_, e = t.MarshalToBuffer(ctx, buffer, 0)
	if e != nil {
		buffer = nil
		return
	}
	return
}

// MarshalToBuffer .
func (t *DogResponse) MarshalToBuffer(ctx core.Context, buffer []byte, use int) (size int, e error) {
	use, e = core.MarshalString(ctx, buffer, use, 1, t.Name)
	if e != nil {
		return
	}
	size = use
	return
}

// Unmarshal0DogResponses .
func Unmarshal0DogResponses(ctx core.Context, buffer []byte) (arrs []*DogResponse, e error) {
	items, e := core.UnmarshalCount(ctx, buffer)
	if e != nil || len(items) == 0 {
		return
	}
	arrs = make([]*DogResponse, len(items))
	for i, item := range items {
		if len(item) != 0 {
			var tmp DogResponse
			e = tmp.Unmarshal(ctx, item)
			if e != nil {
				arrs = nil
				return
			}
			arrs[i] = &tmp
		}
	}
	return
}

// Unmarshal .
func (t *DogResponse) Unmarshal(ctx core.Context, buffer []byte) (e error) {
	var fields [1]core.Field
	fields[0].ID = 1
	e = core.UnmarshalFields(ctx, buffer, fields[:])
	if e != nil {
		return
	}
	t.Name = core.UnmarshalString(ctx, fields[0])

	return
}

// FishRequest .
type FishRequest struct {
	Count	int64
}

// Reset .
func (t *FishRequest) Reset() {
	t.Count = 0
}

// Marshal0SizeFishRequests .
func Marshal0SizeFishRequests(ctx core.Context, use int, arrs []*FishRequest) (int, error) {
	if len(arrs) != 0 {
		var e error
		use, e = core.MarshalSizeAdd(ctx, use, 4)
		if e != nil {
			return 0, e
		}
		for _, v := range arrs {
			use, e = core.MarshalSizeAdd(ctx, use, 2)
			if e != nil {
				return 0, e
			}
			if v != nil {
				use, e = v.MarshalSize(ctx, use)
				if e != nil {
					return 0, e
				}
			}
		}
	}
	return use, nil
}

// MarshalSize .
func (t *FishRequest) MarshalSize(ctx core.Context, use int) (size int, e error) {
	use, e = core.MarshalInt64Size(ctx, use, t.Count)
	if e != nil {
		return
	}
	size = use
	return
}

// Marshal0FishRequests .
func Marshal0FishRequests(ctx core.Context, buffer []byte, use int, arrs []*FishRequest) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	var start int
	order := ctx.ByteOrder()
	for _, v:=range arrs {
		start = use
		use, e = core.MarshalAdd(ctx, use, 2, len(buffer))
		if e != nil {
			return
		}
		if v == nil {
			order.PutUint16(buffer[start:], 0)
		} else {
			use, e = v.MarshalToBuffer(ctx, buffer, use)
			if e != nil {
				return
			}
			order.PutUint16(buffer[start:], uint16(use-start-2))
		}
	}
	size = use
	return
}

// Marshal .
func (t *FishRequest) Marshal(ctx core.Context) (buffer []byte, e error) {
	size, e := t.MarshalSize(ctx, 0)
	if e != nil {
		return
	}
	if size == 0 {
		return
	}
	buffer = make([]byte, size)
	_, e = t.MarshalToBuffer(ctx, buffer, 0)
	if e != nil {
		buffer = nil
		return
	}
	return
}

// MarshalToBuffer .
func (t *FishRequest) MarshalToBuffer(ctx core.Context, buffer []byte, use int) (size int, e error) {
	use, e = core.MarshalInt64(ctx, buffer, use, 1, t.Count)
	if e != nil {
		return
	}
	size = use
	return
}

// Unmarshal0FishRequests .
func Unmarshal0FishRequests(ctx core.Context, buffer []byte) (arrs []*FishRequest, e error) {
	items, e := core.UnmarshalCount(ctx, buffer)
	if e != nil || len(items) == 0 {
		return
	}
	arrs = make([]*FishRequest, len(items))
	for i, item := range items {
		if len(item) != 0 {
			var tmp FishRequest
			e = tmp.Unmarshal(ctx, item)
			if e != nil {
				arrs = nil
				return
			}
			arrs[i] = &tmp
		}
	}
	return
}

// Unmarshal .
func (t *FishRequest) Unmarshal(ctx core.Context, buffer []byte) (e error) {
	var fields [1]core.Field
	fields[0].ID = 1
	e = core.UnmarshalFields(ctx, buffer, fields[:])
	if e != nil {
		return
	}
	t.Count = core.UnmarshalInt64(ctx, fields[0])

	return
}

// FishResponse .
type FishResponse struct {
	Sum	int64
}

// Reset .
func (t *FishResponse) Reset() {
	t.Sum = 0
}

// Marshal0SizeFishResponses .
func Marshal0SizeFishResponses(ctx core.Context, use int, arrs []*FishResponse) (int, error) {
	if len(arrs) != 0 {
		var e error
		use, e = core.MarshalSizeAdd(ctx, use, 4)
		if e != nil {
			return 0, e
		}
		for _, v := range arrs {
			use, e = core.MarshalSizeAdd(ctx, use, 2)
			if e != nil {
				return 0, e
			}
			if v != nil {
				use, e = v.MarshalSize(ctx, use)
				if e != nil {
					return 0, e
				}
			}
		}
	}
	return use, nil
}

// MarshalSize .
func (t *FishResponse) MarshalSize(ctx core.Context, use int) (size int, e error) {
	use, e = core.MarshalInt64Size(ctx, use, t.Sum)
	if e != nil {
		return
	}
	size = use
	return
}

// Marshal0FishResponses .
func Marshal0FishResponses(ctx core.Context, buffer []byte, use int, arrs []*FishResponse) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	var start int
	order := ctx.ByteOrder()
	for _, v:=range arrs {
		start = use
		use, e = core.MarshalAdd(ctx, use, 2, len(buffer))
		if e != nil {
			return
		}
		if v == nil {
			order.PutUint16(buffer[start:], 0)
		} else {
			use, e = v.MarshalToBuffer(ctx, buffer, use)
			if e != nil {
				return
			}
			order.PutUint16(buffer[start:], uint16(use-start-2))
		}
	}
	size = use
	return
}

// Marshal .
func (t *FishResponse) Marshal(ctx core.Context) (buffer []byte, e error) {
	size, e := t.MarshalSize(ctx, 0)
	if e != nil {
		return
	}
	if size == 0 {
		return
	}
	buffer = make([]byte, size)
	_, e = t.MarshalToBuffer(ctx, buffer, 0)
	if e != nil {
		buffer = nil
		return
	}
	return
}

// MarshalToBuffer .
func (t *FishResponse) MarshalToBuffer(ctx core.Context, buffer []byte, use int) (size int, e error) {
	use, e = core.MarshalInt64(ctx, buffer, use, 1, t.Sum)
	if e != nil {
		return
	}
	size = use
	return
}

// Unmarshal0FishResponses .
func Unmarshal0FishResponses(ctx core.Context, buffer []byte) (arrs []*FishResponse, e error) {
	items, e := core.UnmarshalCount(ctx, buffer)
	if e != nil || len(items) == 0 {
		return
	}
	arrs = make([]*FishResponse, len(items))
	for i, item := range items {
		if len(item) != 0 {
			var tmp FishResponse
			e = tmp.Unmarshal(ctx, item)
			if e != nil {
				arrs = nil
				return
			}
			arrs[i] = &tmp
		}
	}
	return
}

// Unmarshal .
func (t *FishResponse) Unmarshal(ctx core.Context, buffer []byte) (e error) {
	var fields [1]core.Field
	fields[0].ID = 1
	e = core.UnmarshalFields(ctx, buffer, fields[:])
	if e != nil {
		return
	}
	t.Sum = core.UnmarshalInt64(ctx, fields[0])

	return
}
