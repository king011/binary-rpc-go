// 定義 包
const pkg = "zoo"

core.RegisterType(pkg, "cat_request").register([
    {
        id: 1,
        name: "id",
        symbol: core.int64,
    },
])
core.RegisterType(pkg, "cat_response").register([
    {
        id: 1,
        name: "name",
        symbol: core.string,
    },
])

core.RegisterType(pkg, "fish_request").register([
    {
        id: 1,
        name: "count",
        symbol: core.int64,
    },
])
core.RegisterType(pkg, "fish_response").register([
    {
        id: 1,
        name: "sum",
        symbol: core.int64,
    },
])

core.RegisterType(pkg, "dog_request").register([
    {
        id: 1,
        name: "count",
        symbol: core.int64,
    },
])
core.RegisterType(pkg, "dog_response").register([
    {
        id: 1,
        name: "name",
        symbol: core.string,
    },
])

core.RegisterType(pkg, "bird_request").register([
    {
        id: 1,
        name: "id",
        symbol: core.int64,
    },
])
core.RegisterType(pkg, "bird_response").register([
    {
        id: 1,
        name: "name",
        symbol: core.string,
    },
])